<a href="https://www.youtube.com/DebianBrasilOficial"><img width="480px" src="assista.png"/></a>

# Debian 30 anos

Todos os anos no dia 16 de agosto (ou no sábado mais próximo) comunidades ao
redor do mundo organizam nas suas cidades o
[Debian Day](https://wiki.debian.org/DebianDay) com o objetivo de celebar o
aniversário do [Debian](https://www.debian.org), sendo um dia totalmente
dedicado a divulgação e contribuições ao Projeto.

**Em 2023 o projeto Debian completará 30 anos de existencia!**

E para celebrar essa data especial, a comunidade Debian Brasil promoverá uma
semana de atividades online.

## Quando?

- 14 a 18 de agosto de 2023.
- Das 19h às 22h.


## Onde?

Canal no YouTube do Debian Brasil:
[youtube.com/debianbrasil](https://www.youtube.com/DebianBrasilOficial)

## Será ao vivo?

Sim.

## Qual o formato das atividades?

Serão palestras ou atividades mão na massa relacionadas ao [Projeto
Debian](https://www.debian.org).

## Calendário das atividades confirmadas

 | seg - 14/08 | ter - 15/08 | qua - 16/08 | qui - 17/08 | sex - 18/08 |
 :---:|:---:|:---:|:---:|:---:
19:00<br>Nova geração: uma entrevista com iniciantes no projeto Debian<br>**Antonio Terceiro, Thais Araujo e Aquila Macedo**<br><br>20:30<br>Instalação personalizada e automatizada do Debian com preseed<br>**Eriberto Mota**|19:00<br>Manipulando patches com git-buildpackage<br>**Lucas Kanashiro**<br><br>20:30<br>debian.social: Socializando Debian do jeito Debian<br>**Giovani Ferreira**|19:00<br>Proxy reverso com WireGuard<br>**Sergio Durigan Junior**<br><br>20:30<br>Celebração dos 30 anos do Debian!<br>**Vários participantes**|19:00<br>Instalando o Debian em disco criptografado com LUKS<br>**Thiago Andrade**<br><br>20:30<br>O que a equipe de localização já conquistou nesses 30 anos.<br>**Charles Melara & Cia**|19:00<br>Debian - Projeto e Comunidade!<br>**Daniel Lenharo de Souza**<br><br>20:30<br>Design Gráfico e Software livre, o que fazer e por onde começar<br>**Jefferson Maier**|

## Mais detalhes das atividades

Data | Horário | Palestrante | Assunto
--- | --- | --- | ---
*14/08/2023* | *19:00* | **Antonio Terceiro, Thais Araujo e Aquila Macedo** |  **Nova geração: uma entrevista com iniciantes no projeto Debian.** Descrição: Terceiro tem quase 20 anos de Debian, e vai entrevistar Thais e Aquila, que começaram a contribuir com o Debian este ano. Vamos discutir as suas motivações, alegrias e frustrações contribuindo com o Debian.
*14/08/2023* | *20:30* | **Eriberto Mota** | **Instalação personalizada e automatizada do Debian com preseed.** Descrição: Preseeding é uma forma de instalar o Debian com base em respostas oriundas de um arquivo de texto. É possível responder total ou parcialmente a todas as perguntas que são feitas durante a instalação, sem a intervenção do usuário. O arquivo com as respostas poderá ser fornecido de várias formas, como por via de um servidor web ou de um serviço DHCP.
*15/08/2023* | *19:00* | **Lucas Kanashiro** | **Manipulando patches com git-buildpackage.**
*15/08/2023* | *20:30* | **Giovani Ferreira** | **debian.social: Socializando Debian do jeito Debian.** Descrição: alguns serviços são mantidos sob o domínio debian.social. O objetivo principal é criar um espaço seguro para os contribuidores Debian (sejam eles membros do projeto ou não) para compartilhar o que estão fazendo no projeto e mostrar seu trabalho, colaborar com os outros e compartilhar conhecimento. Vamos falar um pouco de redes sociais livre e federadas, além de alguns serviços que ampliam, para além das listas de e-mail e do IRC, a socialização no ecossitema Debian. 
*16/08/2023* | *19:00* | **Sergio Durigan Junior** | **Proxy reverso com WireGuard.** Descrição: vamos dar uma olhada em como configurar um proxy reverso utilizando WireGuard. Essa técnica pode ser utilizada pra servir conteúdo a partir de um servidor caseiro.
*16/08/2023* | *20:30* | **Vários participantes** | **Celebração dos 30 anos do Debian!**
*17/08/2023* | *19:00* | **Thiago Andrade** | **Instalando o Debian em disco criptografado com LUKS.** Descrição: O Linux Unified Key Setup (LUKS) é uma especificação de criptografia de disco criada por Clemens Fruhwirth em 2004 e originalmente destinada ao Linux. Iremos demonstrar como instalar o Debian com criptografia de disco utilizando LUKS com LVM (Logical Volume Management). A criptografia de disco é essencial nos dias atuais protegendo seus dados caso seu notebook/disco seja violado.
*17/08/2023* | *20:30* | **Charles Melara & Cia** | **O que a equipe de localização já conquistou nesses 30 anos.** Descrição: A equipe de localização (a.k.a. tradução) está ativa há mais de 25 anos. Vamos descobrir como e quando essa história começou e também ver todos os marcos dessa jornada.
*18/08/2023* | *19:00* | **Daniel Lenharo de Souza** | **Debian - Projeto e Comunidade!** Descrição: O Debian foi lançado em 1993 para ser um Sistema Operacional Livre. Para que o sistema possa acontecer, uma comunidade se formou para sustentar o projeto. Nesta atividade, será apresentado um pouco do que é o projeto e a comunidade.
*18/08/2023* | *20:30* | **Jefferson Maier** | **Design Gráfico e Software livre, o que fazer e por onde começar.** Descrição: o mundo do design gráfico geralmente é tanto uma pedra no sapato para quem quer migrar para Debian e outros sistemas, como também uma das dificuldades das comunidades para desenvolver seus trabalhos com qualidade. Nessa atividade serão apresentados os principais softwares, um pouco da comunidade livre e open, casos de sucesso e muita inspiração!

## Código de conduta

Iremos seguir o [Código de Conduta
Debian](https://www.debian.org/code_of_conduct) e o
[Código de Conduta da DebConf](https://www.debconf.org/codeofconduct.shtml)

## Arquivos usados nas apresentações

Data | Arquivo
--- | ---
*14/08/2023* | [Instalação personalizada e automatizada do Debian com preseed](http://eriberto.pro.br/palestras/preseed-debian.pdf)
*15/08/2023* | [Manipulando patches com git-buildpackage](arquivos/git-buildpackage-presentation.md)
*15/08/2023* | [debian.social: Socializando Debian do jeito Debian]( arquivos/debian-social.pdf)
*16/08/2023* | [EM BREVE Proxy reverso com WireGuard]( arquivos/.)
*17/08/2023* | [Instalando o Debian em disco criptografado com LUKS](https://andrade.wiki.br/wp-content/uploads/2023/08/luks-debian.pd)
*17/08/2023* | [O que a equipe de localização já conquistou nesses 30 anos]( arquivos/Historico_da_equipe_de_traducao.pdf)
*18/08/2023* | [Debian - Projeto e Comunidade!]( arquivos/Debian-Projeto_Comunidade.pdf) [.odp]( arquivos/Debian-Projeto_Comunidade.odp)
*18/08/2023* | [Design Gráfico e Software livre, o que fazer e por onde começar]( arquivos/design-grafico.txt)
