# git-buildpackage para iniciantes
Como manter seu pacote Debian com git

---

# whoami

* Desenvolvedor Debian e Ubuntu
* Integrante do grupo Debian Brasília
* Engenheiro de software na Canonical

---

## git-buildpackage

```
Suite to help with maintaining Debian packages in Git repositories
```

Algumas funcionalidades:

* Importar versões do `upstream`
* Re-criar `tarballs` do `upstream` a partir do conteúdo do repo git
* Assinar `tags` automaticamente
* Executar `hooks`
* Gerar `changelog` automaticamente
* Gerenciar `patches`

### Mais informações

https://wiki.debian.org/PackagingWithGit

https://honk.sigxcpu.org/piki/projects/git-buildpackage/

---

# Teminologia e Layout do repositório

## Branches

* **debian-branch** (`main`/`debian/sid`/`master`): contem o trabalho de
  empacotamento
* **upstream-branch** (`upstream`): contem a release do upstream
* **pristine-tar branch** (`pristine-tar`): contem informação para
  re-criar o tarball do upstream
* **patch-queue branch** (`patch-queue/main`): conteúdo da debian-branch
  com os patches aplicados

---

# Configuração local do git-buildpackage

Pode ser configurado a nivel do sistema ou por usuário.
Para configurar para o seu usuário, crie o arquivo `~/.gbp.conf`.

## Mais informações

http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.cfgfile.html

https://manpages.debian.org/testing/git-buildpackage/gbp.conf.5.en.html

---

# Exemplo de conteúdo do `~/.gbp.conf`:

```
[DEFAULT]
# the default build command
builder=sbuild
# the default branch for upstream sources
upstream-branch=upstream
# the deafult branch for debian packaging
debian-branch=main

[buildpackage]
# Sign tags with GPG
sign-tags=True
# Key ID to sign tags with
keyid = 0xdeadbeef

[import-orig]
# Update pristine-tar branch
pristine-tar=True

[dch]
git-log=--no-merges
```

---

# Casos de uso

* Clonar um repositório existente
* Criação de reposirótio git
* Gerenciamente de patches
* Geração de changelog
* Importar versão nova do upstream
* Construindo um pacote

---

# Clonar um repositório existente

```bash
$ gbp clone <url_repo_git> --pristine-tar
```

---

# Exemplo criação de repositório

```bash
$ gbp clone https://salsa.debian.org/ruby-team/jekyll.git --pristine-tar
gbp:info: Cloning from 'https://salsa.debian.org/ruby-team/jekyll.git'
$ cd jeykyll
$ git branch
* master
  pristine-tar
  upstream
```

---

# Criação de repositório git

## Pacote novo

Cria-se o pacote localmente (sem usar git), usando sua ferramenta de
preferência (i.e. `dh_make`, `gem2deb`). Assim que estiver pronto,
construindo sem problemas, crie o repositório git a partir do aquivo
`*.dsc` gerado:

```bash
$ mkdir <pkg> && cd <pkg>
$ git init
$ gbp import-dsc <caminho_para_arquivo_dsc> --pristine-tar
```

## Pacote existente

```bash
$ mkdir <pkg> && cd <pkg>
$ git init
$ gbp import-dscs --debsnap --pristine-tar <pkg> 
```

---

# Gerenciamento de patches

A branch `patch-queue` será usada para aplicar os patches em
`debian/patches` no topo do conteúdo da `debian-branch`. Para
importar os patches basta executar o seguinte comando a partir
da `debian-branch`:

```bash
$ gbp pq import
```

Cada patch em `debian/patches` se torna um commit, onde podem ser
manipulados como desejar (remover, modificar, adicionar).

Quando todos os patches/commits estiverem pronto na branch
`patch-queue` basta exportá-los para a `debian-branch` novamente:

```bash
$ gbp pq export
```

---

# Exemplo de criação de patch

```bash
$ git branch
* master
  pristine-tar
  upstream
$ ls debian/patches/
00-fix-whatis.patch  01-fix-typos-in-manpages.patch  series
$ gbp pq import
gbp:info: Trying to apply patches at '4b32812a1a793296ff3fd845a55c0831624caac1'
gbp:info: 2 patches listed in 'debian/patches/series' imported on 'patch-queue/master'
$ git log -n 3 --pretty=oneline
9d866a4441ab1c714a766452539fc9262703d9f9 (HEAD -> patch-queue/master) Fix typos in manpages
9c531dc646593e7c288d1f5c8a81e833868a91e7 Fix whatis entry in srec_input.1
4b32812a1a793296ff3fd845a55c0831624caac1 (tag: debian/1.64-4, origin/master, origin/HEAD, master) Merge branch 'master' into 'master'
$ echo "Testing gbp-pq..." >> README
$ git add README 
$ git commit -m"Testing gbp-pq"
[patch-queue/master ddf9e03] Testing gbp-pq
 1 file changed, 1 insertion(+)
$ gbp pq export
gbp:info: On 'patch-queue/master', switching to 'master'
gbp:info: Generating patches from git (master..patch-queue/master)
$ ls debian/patches/
0003-Testing-gbp-pq.patch  01-fix-typos-in-manpages.patch
00-fix-whatis.patch        series
$ git add debian/patches/0003-Testing-gbp-pq.patch debian/patches/series 
$ git commit -m'Add patch testing gbp-pq'
[master a0fe179] Add patch testing gbp-pq
 2 files changed, 18 insertions(+)
 create mode 100644 debian/patches/0003-Testing-gbp-pq.patch
```

---

# Geração de changelog

Faça suas modificações atômicas em commits e quando quiser atualizar
o changelog execute o seguinte comando da `debian-branch`:

```bash
$ gbp dch
```

Caso já deseje fazer a release do pacote pode executar (o target será
atualizado para o `unstable` por padrão):

```bash
$ gbp dch --release
```

---

# Exemplo de geração de changelog

```bash
$ git log -n 5 --pretty=oneline
a0fe179f2c6bf45db453dbc87333f72187fa711e (HEAD -> master) Add patch testing gbp-pq
4b32812a1a793296ff3fd845a55c0831624caac1 (tag: debian/1.64-4, origin/master, origin/HEAD) Merge branch 'master' into 'master'
ca1d13e6b86d22f5411352e3a0f6d778469f0af0 (aquila/master) Release version 1.64-4 to unstable.
e6756f2959963cce0327680991e8d40d9a1fc0e8 New changelog.
6f46693a30467c3347fbb443dea9904fe110b925 debian/patches: Created 01-fix-typos-in-manpages.patch to fix typos in the manpages.
$ gbp dch
gbp:info: Found tag for topmost changelog version '4b32812a1a793296ff3fd845a55c0831624caac1'
gbp:info: Continuing from commit '4b32812a1a793296ff3fd845a55c0831624caac1'
$ head -n 8 debian/changelog 
srecord (1.64-5) UNRELEASED; urgency=medium

  * Add patch testing gbp-pq

 -- Lucas Kanashiro <kanashiro@debian.org>  Tue, 15 Aug 2023 18:09:09 -0300

srecord (1.64-4) unstable; urgency=medium

```

---

# Importar versão nova do upstream

Considerando que o `debian/watch` do pacote está funcionando perfeitamente,
basta executar o seguinte comando a partir da `debian-branch` para importar
uma versão nova do upstream:

```bash
$ gbp import-orig --uscan
```

Caso haja algum problema com a execução do comando acima, tente executar o
`uscan` no modo verboso para identificar o problema e saná-lo:

```bash
$ uscan --verbose
```

---

# Construindo um pacote

Para construir um pacote basta executar o seguinte comando a partir da
`debian-branch`:

```bash
$ gbp buildpackage
```

Por padrão, ele tentará executar `debuild -i -I`. Caso queira utilizar outro
builder, como por exemplo o `sbuild`, pode executá-lo da seguinte forma:

```bash
$ gbp buildpackage --git-builder=sbuild -d unstable
```

Lembrando, que o `git-buildpackage` irá utilizar na construção do pacote apenas
modificações que estejam *commitadas* no repositório.

**Off-topic**: Para informações de como configurar o `sbuild` na sua máquina
local, temos uma página em português na wiki do Debian Brasília:

https://wiki.debianbsb.org/pt-br/howto/sbuild

---

# QA

Obrigado!
